General
-LOTS of reordering things, adding more info on things in places to find them easier. 
-new! layout that feels more narratively appropiate for the game

The basics of Play:
-Added Inspiration System, to allow more rerolls when in a dire situation
-Changed Conflict and Non-Conflict rolls to Opposed and Unopposed rolls
-Skills are talked about here now!

Skills: 
Skills now can have one of three tags: Reactive, Passive, or Active. There is now a section about what each of these three things do.
-Addtional Skill updates to Jobs and Creatures are covered in their respective sectiions of this update guide.

Character Creation:
-a list of the basics of jobs are here, so you can stat your character without going to the back of the book and reading jobs before going back to this page

Starting the game:
-a small 'blurb' is added about how to start the game and creates a section for topics that are not only in a journey, such as skills, keepsakes, and the expereince system, the latter which has been moved from the 'completeing the journey' part of the book

Keepsakes:
-Added info about Consumable Keepsakes, which you can use regardless if they are equipped or not
-added crafting system! This is the biggest mechanic update.
-added rarities to all Material keepsakes.
-new material keepsakes! They are [Material] [Restorative] [Medicinal] [Shining] [Absorbing][Fertilizing] 
-[Melee] [Ranged] Cannot be combo'd, cannot be in slot one (unless we change how things work with this)
-[Risky Odds] [Regular Odds][Reliable Odds] now allow rerolls in Conflict equal to its Rarity.
-immaterial keepsakes got a major overall. the four are now [people] [place] [thing] and [motive]
-[motive] is what will make alternative win conditions now

journeys:
-Added information about Exhaustion: what happens when everyone is Subdued
-What happens when you give up a journey is also covered in this section.

Events
-Updated Events during Travel to create decisions, rather than a strict bad/good without any player contribution
-Clarified Negative and Positive Events, as well as where they are on the Events list 
-updated events to match new mechanics, ie; motive keepsakes

Conflict:
-Reworded Alternative Win Conditions, referancing Motives, and how there is a 'standard win condition'
-adjacency is defined now as "the zones adjacent to the one your current zone is touching"
-Ranged weaponry now allows attacking adjacent zones only; characters may no longer move diagonally
-Added use of Consumables in the 'interacting with Keepsakes' section
-moved how sources are used in opposed situations to the Spending Sources session
-Characters gain a point of Fatigue after combat per 6 rounds they spent in conflict instead of 12 now
-Added a new mechanic where you gain a third action, but cannot act next round (nor can be chosen to act in the next round)

Rest:
-Added 'Craft a Keepsake' to Resting
-Hunting for Food is now Risky for Protectors (to give all three types two good and two bad tasks)
-Added when Crafting a Keepsake happens when resolving rest
-added info about expereinces in rest so its easier to quick referance 

Jobs:
-now have a section in character creation that is a 'taste' of what each job is like. Full job information is now at the back of the book
-skills updated with new keywords and zone updates.
-Narrative descriptions of Skills have been reworded to fit with the skill type, ie, Fertilize as a ractive skill now having the description of "The user scatters soil at their feet reflexively in a pinch." instead of "The user uses their cultivating know-how to enrich the soil at their feet."
-notable skill updates:
-Removed Flavor text in the conflict part of Plant Moonflower and Sunflower.
Snipe: The user spends a Source of Shade to make an Attack Action on an opponent in a Zone Adjacent to the user’s Adjacent Zones.

Creatures:
-removed 'alternative win conditions' from enemies (baked some into some of their descriptions)
-Added a 'penalty' or 'bonus' to each creature, replacing 'x to all rolls'. These are now based on the creature's rating (-2, -1, 0, 1, and 2 for ratings 1-5 respectively) instead of being based on what keepsake they use.
-keywords have been added to all skills.
-Creatures who have changed their builds outside of the penalty/bonus overhaul are as follows:
-Starstalkers: Penalty lowered to 0
-Greater Mosscoat have the [Shining] Attribute on their Keepsake
-Orchid Mantis has a Keepsake with [Absorbing]
-The Roamer of the Fields now has two keepsakes; melee with Fertilizing, and a ranged keepsake with nothing else on it

-updated skills:
 Manifest Night: Starstalkers may use an Action to turn one source of Shade within their current an adjacent Zone into two sources of Shade.

Cry or Help: This Leporythm spends an Action to Cry for Help. Until their next turn, ally Leporythm in Zones adjacent to the Zones that are adjacent to this Leporythm's current zone may move to this Leporythm's current zone using only one Move Action. 
 
Roost: While Tender is in the same zone as this Rooster, Rooster does not need to roll 
Support in order to heal a status from themself.

TLC: Tender spends a Source of Soil in their Zone, healing an ally in Tender's current or adjacent Zone of Restrained Status and gives that ally +2 to their next Defense roll.

Living Garden: When Moss Gargoyle does not use the Move Action during their turn, their allies may treat them as a source of Soil until the start of this Moss Gargoyle's next turn. This effective source of Soil cannot be removed,and will not be consumed when spent as a cost for skills or bonuses. 
