3.1 Update List:

Combat:
-A Standard Physical Attack has been given tags to understand how to use the action.
-Initiative has been explained in Turn Order.
-Advantage now suceeds if a party draws a card witha difference of 5 or more; a party that draws the higher card but does not have a 5 value differance continues the combat as normal.
-Buffs and Debuffs now last until the start of the caster's turn -- new intances of a buff (or debuff) shift which caster to count turn order with. (ie: if Character A uses Rakunda on an enemy turn 1, it would wear off on that enemy at the start of Character A's 4 turn; however, if Character B were to then use Armor Splitter on that same enemy, not only does the debuff wear off by turn 5, but it wear off on Character B's turn, not A.)
-Status is only 1 at a time. A new status will ovveride another status with the exception of Knocked Down, which makes a character have both status, and Knocked Out, in which once you have Knocked Out, you cannot replace it with a new status alltogether. 

Skills:
-All 3-tier skills that have a secondary effect reliant on Ail Evasion (ie: Agi with Burn) have been explained proper in the skill; for example, to burn an enemy, you must meet both Atk and and Ail Evasion in order for the move to burn in addition to dealing 1+SPEC damage. 
-Amrita Drop and Purfiying Rain reworded to note that it cannot cure Knocked Down or Knocked Out. 
-Element Wall and Break are now a Buff and Debuff rexpectively. (as such, they now can be dispeled with the means of Skills and Items, such as Dekaja.)
-Clarified the interaction with Ultra Charge and the Focus Line Skills; neither can stack on each other. 
-Spotlight and Healing Breeze reworded to better explain their interation with the turn order. 
-Third Eye allows you to see all enemies' weaknesses.
-Dinivation's cost is now lowered to 2.
-Warding Prayer's cost is updated to 6, and can now halt the Reaper's movment between rooms for 2 turns. 

Items:
-Ofudas boost the proper stats by 1, rather than the older version's +3. 
-Medigo Bomb now does 'damage', rather than 'almighty damage'. 
-Recover Oil does not cure Knocked Out or Knocked Down.
-Bands now read as DEX+1 rather than AG+3, for example.

Other:
-Changed Physical to Strike for various intances
-Changed Instances of Light and Dark to Curse and Bless
-general updates of typos and awkward spacing
-The World may now level any Bond instead of an existing one. 
-Bosses have been given a number of skills that they alone can take. These skills nullify the effects of a status, but still allow for Technicals and even All-Out Attacks. 
