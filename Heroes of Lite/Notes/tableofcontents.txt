Introduction		03
What is Fire Emblem?	04
The Basics		05
Stats			05
Character Creation		07
Weapon Proficiency		o8
Physical			08
Magicial			09
Special 			10
Starting Weapon Chart	11
Movement Types		12
Distributing Stats 		13
Skills			15
Personal Skill Examples 	16
Leveling Up		17
Weapon Refinement		18
Playing The Game		19
Using Out of Combat Actions	20
Supports			21
Combat			22
Phases			22
Initiating Combat		23
Using Action Skills		25
Using Staff Effects		25
Rescuing Units		25
Trading and Using Items	25
Waiting			25
Barrier and Multi HP Units	26
Terrain			27
Siege Weaponry		27
Fog of War		28
Status Effects		29
Stabilization		29
Optional Rulesets		30
Post-Battle		31
Indexes			32
Skills			32
Items			43
Weapons			44
GMing: Tips and Tricks	59
Creating Chapters		61
Map Making		63
Preset Units		67
Credits			70
Stat Sheets		71