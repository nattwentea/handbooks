Anima
name		cost	might	range	refine 1		refine 2
sagittae		700	2	1-2	[cheap]		[Enchanted Def]
Fire		1100	4	1-2	[Beastslaying]
Wind		1100	4	1-2	[Wingclipping}
Thunder		1100	4	1-2	[Dragonslaying]
Elwind		1400	6	1-2	[El]		[Wingclipping]
Arcthunder	1500	7	1-2	[Dragonslaying]	[Arc]
Corvus Tome	1600	6	1-2	[el]		[Adaptive]

Light
name		cost	might	range	refine 1		refine 2
Nosferatu		1300	2	1-2	[Cheap]		[Resire]
Shine		700	2	1-2	[Cheap]		[Enchanted Speed]
Purge		1500	4	1-3	[Monsterslaying]	[Blessed]
Seraphim		1500	7	1-2	[Monsterslaying]	[Arc]
Valaura		1300	7	1-2	[Posion]		[Arc]


Dark:
name		cost	might	range	refine 1		refine 2
Night		700	2	1-2	[cheap]		[Enchanted Res]
Waste		1200	2	1-2	[cheap]		[brave]
Swarm		900	2	1-3	[cheap]		[Blessed]
Mire		1100	4	1-2	[Armorslaying]
Apocalypse 	1200	9	1-2	[El]		[Arc]
Ruin		1800	4	1-2	[Enchanted skill]	[Killer]
					
Stones:
name		cost	might	range	refine 1		refine 2
Powerstone	800	8	1	[Greater]
Strangestone	1000	4	1	[cheap]		[Oddly-Glowing]
Smoothstone	1000	6	1	[Even-stronger]
Blessedstone	1100	6	1	[Monsterslaying]
Divinestone	2000	6	1	[dragonslaying]	[Even-brighter]

Staves:
name	cost	might	h range	stat ran	refine 1	refine 2
Mend 	800	10	1	----	[Mend] 	---
Rescue	1100	5	1	1-7	[Rescue]
Torch	800	5	1	---	[Torch]
Miswarp	1100	5	1	1-7	[harmful]	[Warp]
Mis-Sleep	1100	5	1	1-7	[sacrifical]	[Sleep]
Catharsis 	1800	10	1-7	---	[Mend]	[physic]

