
Mechanics Updates:
-Supports are now based on the unit's movement type, with infantry getting whichever they want.
-Non-Combat skills are stat/5 (rounded down) or 3, whichever is lower.
-Stablixation is additive, but only up until 4 maps.
-added a small note on Post-Combat damage on page 22. 

Refine Updates:
-Even Stronger: Deals 4 Post-Combat Damage on even turns. 
-Oddly-Shaped: Deals 3 Post-Combat Damage on even turns. 

Skill Updates:
-Added Stalwart Gaurd and Steady Heart, for more GM-only options.
-Quixotic - Hit+3 and Avoid-3 for the User.
-Brash Assault - Effect: If user has 1/2 or less HP and initiates combat against an opponent that can counterattack, the user performs a Follow-Up Attack regardless of Speed. 
-Triangle Attack - Strategy: The user’s next attack automatically hits and Critical hits, but cannot perform a Follow-Up Attack. This skill can only be used when two other units with Triangle Attack are each in a Cardinal Direction of the target, are all of equal distance from the target, and are all no further than 2 spaces away from the target.  
-A diagram has been added for Triangle Attack 
-Guidance: Infantry and Armor allies within the user’s Movement Range can move to a space adjacent to this unit (the moving unit spends all of their Movement doing this). The moving unit can perform their action after moving.
Bane: The user’s Dagger attack gains an additional +1 hit per X Charge spent, up to a maximum of 5 extra Hit. If the user can strike multiple times (i.e. due to Follow-Up Attacks or a Brave refine) and wants to, the user must spend the required charge per strike before combat starts.
-Curved Shot: The user’s Bow attack has an additional 1 Range per X charge spent, up to a maximum of 5 Range total. If the user can strike multiple times (i.e. due to Follow-Up Attacks or a Brave refine) and wants to, the user must spend the required charge per strike before combat starts.
-Seafarer: Water becomes Rough Terrain to this unit.
-Fortune's prereq is now Level 25.
-Pickpocket now costs 3 Charge.
-Deep Bite is now 2 Charge. "The user doubles all Post-Combat damage they would deal to the opponent after this combat."
-Self-Destruct: The user explodes, dealing damage equal to their Attack stat to everyone within 2 spaces (Ignoring Defense and Resistance). User becomes KO’d, and their recovery time is set to 3 maps.
-Elbow Room: When user fights in terrain with no terrain effects, +2 Damage during combat.

Clarifications:
-Turned instances of 'declaring Combat' to Initating Combat 
-Clarified weapon advantage and disdvantage on pages 22 
-Clarified Critical Hits
-Clarified Post Combat

Typos:
-Silencer's Prereq: 'user uses Daggers'
-Aid is from by a unit's 'Strength + Movement Type' 
-Killer Refine: 'When in combat against this unit, the opponent has 5 less Critical Avoid'

Other:
-added information on Strike, Breath, Bows, and Staves for cross referancing on the weapon page.
