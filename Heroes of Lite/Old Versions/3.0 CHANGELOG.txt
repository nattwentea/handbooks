General:
-Updated Wordage on pages, such as What is Fire Emblem, to reflect Gameplay style
-Heavily revamped Character Creation pages, esepcially Races, to improve ability to quick stat characters
-Players wanting to be Monsters must take Monstrous at creation to qualify. (due to them being created with the purpose of being enemies, players should ask their GM before making them)
-Claws and Curses renamed from 'Phyiscal Monster Weapon' and 'Magical Monster Weapon', which were found the GM section prior. Curses are also now only 1 range.
-Infantry have options outside of Shove to take on creation. 
-Moved Skills List to the back of the guide, before the GM pages, to reflect a 'index section' of the sorts. 
-Remaining section on Skills in CC has personal skills on the next page.
-Moved Refinement list to the back of the guide for a similar purpose, combining them with the weapon list and refinement pricing (in the GM guide) to create an all purpose 'weapon creation and purchasing' section.
-added a section called 'playing the game' and 'after combat' to flow the discussion of gameplay from off battle, to battle, and afterwards. 
-Gming section heavily updated to explain when to give players weapons/options for forging refinements, enemy spread, boss spreads, and when to include barriers/multiple HP bars, denoted as 'markers'.
-explains about enemy transparency, encouraging infomration to be given as needed due to the nature of being a Fire Emblem based game. 
-Preset Units completely reworded and given recommended skills based on level.
-Removed the divide for 'further resourses' due to the removal of a guide on building monster enemies.
-updated character sheet in the book
-gutted all images for BW version -- images will be placed in the future once we commission artists. 

Stats:
-Skill is now Dexterity
-Dexterity is now Arcobatics. 
-Added Finesse, a non-combat stat based on Dexterity.
-Formally added Critical Avoid, which is found by Avoid+15
-Added Gauge to the stat list. 
-Non-combat skills now range from 0-3. At creation, you have 6 points to put into them, and reflect the number of d20 you roll. (a 0 denotes disadvantage).
-Laguz who transform get a static +2 to Size.
-Armored units do not get +1 to a choice of  Def/Res/HP every 5 levels. They do, however, still have the at creation stat bonuses.
-Leveling up only gives one Skill per 5 Levels. 

Combat:
-section overhauled to explain various factors.
-Details the order of 'start of turn' effects.
-notes Transform being a free action, but only if no actions have been taken (after moving)
-Renamed attaching section to 'declaring combat'. Details, Effective Weapon updates (Might x3), Supports, as well as retaining past information. Specials section revamped to Combat Arts.
-Reworded Staves and usage, highlighting Effects.
-Using Skills being renamed to 'using an action skills' to be more specfic. 
-dismounting is gone. 
-Added waiting.
-New section on two enemy  mechanics -- Units with Barriers and Units with Multiple HP Bars.
-Barriers function simuliar to FE3H, with a few differances, to allow GMS to create towering foes who must have their barriers broken in order to do the most damage to them.
-Multiple HP bars are saved to story relavent Bosses and are sparse in a campaign, but can make prolonged battles that incentivise GMs to reveal skills as they lose an HP bar.
-Terrian is now seperated into 'Normal', 'Rough', 'Difficult', and 'Impassable'. Added 'Wall' terrian that cannot be destoryed, and renamed 'destoryable' Walls as 'Cracked Wall'
-Fog of War rehauled to involve revealing and hiding 'sections' instead of a number of tiles. 
-Siege Weaponry reworded as being something you find on the battlefield, instead of a weapon that's carried around at all times. Have both Physical and Magical Variants, and can have a number of attributes. 
-Status Effects slightly reworded to include clarification on number of status and recovery
-Death renamed to stabilization. Until the map ends, a unit can be stabilized, and if not, they simply gain the max number of penalities for future maps.
-An optional 'classic mode' is added for those who want permadeath, and sticks to the former '3 turns or you're out' ruleset. 

Skills:
-Skills come in five types. Passive, Action, Strategy, Technique, and Reflex. This determines when the come into play during combat. Skills have been updated appropiately to reflect when they are active. 

All-Access Skills: 
-Pivot has been added for level 0.
-Stat+2 and Celerity have the added caveat of 'cannot be negated', such as with Parity.
-Dual Wield can now be taken a second time if Infantry.
-Added Flashing Blade and Heavy Blade, which influence charge gains on player phase.
-Fortune is now level 20. 
-Nihil is GM onl and now says, "The opponent cannot activate any Passive, Strategy, Technique, or Reflex skills against this unit." 
-quickened pulse is now "All Combat Arts requiring 3 or more total charge to use require one less charge to activate."
-Excepting Perform and Inspiring Performance (now level 20), all Perform related skills have been removed. Replacing them are Performance of the Focused, Fortunate, and Stalwart.
-Steal allows you to take money if the opponent has no items to take.
-Keen Vision updated to reflect Fog of War changes

Weapon Based Skills:
-Faire skills now have Hit+1 as well. Cannot be taken with Dual Wield.
-Breakers effect the triangle they are good at, instead of the weapon they are. Tome, Dagger, and Bow follow the Fates weapon triangle in this particular instance. 
-Strike and Breath Weapons now have Boon and Vigor -- effecting the status and gauge of adjacent units respectively.
-White Tide is now Luck and Speed.
-Wrathful staff allows staves to initatie combat.

Monster Skills:
-Added Monstrous, a manditory skill that allows units to become Monsters. Can gain access to Claws or Curses at level 1, or later on through means of taking dual weild, in this way. Cannot be Negated.
-Divide disallows HP gain when a clone is on the field (from both the user and the clone). 
-Shadow Gambit reworded. "This unit ignores all terrain Effects and Movement Penalties, including damage taken from terrain. 
-Anathema now gives the penality of taking double damage from light magic. 

Movement Type Skills:
-Arcobatics is now renamed 'Arcobat'
-Various terrian skills (mountain climber, seafarer) reworded with Terrian keywords. 
-Armors now have access to Armored, Warding, and Watchful stance. 
-Cavalry now have access to Armored, Warding, and Precision Blow
-Trample effects the movmeent type instead of if the unit is on a mount or not, as mounts are flavor text. 

Combat Arts:
-Specials have been renamed Combat Arts, which can be taken as early as level 0. 
-Following Specials are now Combat Arts: Imbue, Pavise, Aegis, Miracle, Astra, Dragon Fang, Ignis, Glacies, Counter, Ice Mirror, Sol, Luna, Vengeance, Triangle Attack, Dark Spikes, Upheaval, Heavenly Light, Glaeforce, Eclipse (Formally called Black Luna), Aether, and Fire Emblem 
-Most of these prior specials have had their charge adjusted. 
-All other Specials in prior versions have been removed.
-The following Skills are now Combat arts: Beastly Ward, Gamble, Parity
-New Combat Arts: Pickpocket, Bless, Guardian, Curved Shot, Bane, Multitask, Silencer, Sorcery Blade, Cease Conflict, Knightkneeler, Helmsplitter, Grounder, Bane of Monsters, Roar, Vortex, Deep Bite, Subdue, Exhaustive Strike, Atrocity, Finesse Blade, Alert Stance, and  Revitalize.

Refinement:
-Refined weapons have 'Attributes' attached to them, as opposed to 'Refines'.
-Bows and Staves have 'innate' Attributes of Winglipping and Heal respectively.
-Staff Attributes have 'Effects' attached to them that can be used as a Whole Action. 
-Horseslaying has been combined until Beastslaying, effecting "Cavalry units, and Units equipped with Beaststone and Strike Weapons (Transformed)"
-Mosnterslaying is now Fiendslaying

Attribute Updates:
-Added Cheap and Sealed for GM purposes.
-Seige is gone!
-Steel, Rotten is now +3
-Silver, Fetid is now +4
-Killer is now 'opponent has 5 less Critical Avoid'
-Daggers now have Posion, Incapacitating, and Lethal. Removed Brave, Dragon/Fiendslaying
-Bows have Short, which allows them them to be 1-2 (1-3 with Long). Removed Armorslaying. 
-El and Arc are renamed to Elder and Arcane.
-Anima now has Adaptive, which gives WTA to weapons without Triangle Advantage.  Removed Armorslaying and Brave.
-Light only has Fiendslaying for slaying Attributes. Added Poison and Resire, removed Brave.
-Dark only has Armorslaying for slaying attributes. Gains Killer.
-Staves gain Warp. Moved rescue to basic attributes. Torch Attribuate reworded to match updated Fog of war mechanics.
-Harmful and Sacrifical for GM use with staves.
-Added Weapon 'Promotion' for Strikes and Breaths in appropiate weapon section for easy finding.  
-Curses now have Stone, while Half is moved to Advanced.  
-Siege Weaponry has a Refinement list. Cannot be bought as they are found on maps, instead of brought. 

Items
-Added Tonics
-Updated Torch's usage
-Various price changes and uses for non healing items (Shine Barrier, Mine)
