Hi, and thank you for purchasing this asset pack!

--INTRODUCTION--
This asset pack was made by Cecil (subomieart @ twitter/tumblr) for use with Nat Twentea Tabletop RPG "The Masks We Wear". The game is available to download for free in our Google Drive--if somehow you've purchased these card backs and haven't yet downloaded the game, feel free to download it at our Google Drive:

https://drive.google.com/drive/folders/1HGxoyDBc8hzHDO6jJvxHzp6sd_GFO2SU?usp=sharing

With regards to the sprites, 5 different colors are available: Teal, Purple, Pink, Beige, and Lime. Each color has three variations: one universal/blank, one for the Major Arcana, one for the Minor Arcana.

--TERMS OF USE--
These sprites are made for use in Roll20 using Roll20's ability to upload your own card decks, but of course, feel free to use them however you'd like. You can also edit them for your own purposes if need be, but redistribution is prohibited.

Thanks, and have fun playing!